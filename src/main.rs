use std::{collections::HashMap, convert::Infallible, fmt::Display, fs::File, io::BufReader};

use clap::Parser;
use reqwest::header::{HeaderMap, HeaderValue};
use serde::{Deserialize, Serialize};
use serde_json::{Map, Value as JValue};
use warp::{
    filters::{body, header, path::path, query::query},
    http::StatusCode,
    reply::with_status,
    Filter, Reply,
};

#[derive(Parser)]
#[command(author, version, about, long_about = None)]
struct Cli {
    #[arg(default_value_t = String::from("/etc/dns-updater/conf.json"))]
    config_file: String,
}

// identifier and zone_identifier are path parameters:
// https://api.cloudflare.com/client/v4/zones/{zone_identifier}/dns_records/{identifier}
//
// headers:
// "Authorization: Bearer {api_token}"
// "Content-Type: application/json"
#[derive(Serialize, Debug)]
struct CloudflareUpdateRequest {
    #[serde(rename = "content")]
    ip_addr: String,
    #[serde(rename = "name")]
    domain_name: String,
    #[serde(rename = "proxied")]
    use_proxy: bool,
    #[serde(rename = "type")]
    record_type: String,
    comment: String,
    tags: Vec<String>,
    #[serde(rename = "ttl")]
    time_to_live: u32,
}

#[derive(Deserialize)]
struct CloudflareRecordListing {
    id: String,
    // this is the complete domain of the specific record.
    name: String,
    #[serde(flatten)]
    _unused: Map<String, JValue>,
}

#[derive(Deserialize)]
struct CloudflareListingResponse {
    success: bool,
    result: Vec<CloudflareRecordListing>,
    #[serde(flatten)]
    _unused: Map<String, JValue>,
}

#[derive(Deserialize)]
struct Config {
    addr: [u8; 4],
    port: u16,
}

#[tokio::main]
async fn main() {
    #[cfg(debug_assertions)]
    println!("WARNING: you are running a debug build!!!\n\tthis may output sensitive information into your logs.");
    println!("starting dns updater.");

    let cli = Cli::parse();

    let config: Config = match File::open(cli.config_file) {
        Ok(file) => match serde_json::from_reader(BufReader::new(file)) {
            Ok(config) => config,
            Err(_) => Config::default(),
        },
        Err(_) => Config::default(),
    };

    if cfg!(debug_assertions) {
        // debug path

        let update_route = warp::any()
            .and(path("update"))
            .and(query::<HashMap<String, String>>())
            .and(header::headers_cloned())
            .and(body::bytes())
            .and_then(update_handler_debug);

        println!("now listening on {}", config);
        warp::serve(update_route)
            .run((config.addr, config.port))
            .await;
    } else {
        // release path

        let update_route = warp::any()
            .and(path("update"))
            .and(query::<HashMap<String, String>>())
            .and_then(update_handler);

        println!("now listening on {}", config);
        warp::serve(update_route)
            .run((config.addr, config.port))
            .await;
    }
}

// update version of update_handler dumps incoming request headers, body and query
async fn update_handler_debug(
    q: HashMap<String, String>,
    headers: HeaderMap,
    body: warp::hyper::body::Bytes,
) -> Result<impl Reply, Infallible> {
    println!(
        "headers: {:#?}\nbody: {:#?}\nquery: {:#?}",
        headers, body, q
    );
    update_handler(q).await
}

async fn update_handler(q: HashMap<String, String>) -> Result<impl Reply, Infallible> {
    println!("dns update requested...");

    let zone_ident = match q.get("zone_identifier") {
        Some(val) => String::from(val),
        None => {
            eprintln!("request failed: no zone identifier specified.");
            return Ok(with_status(
                "no zone identifier specified.",
                StatusCode::BAD_REQUEST,
            ));
        }
    };

    let api_token = match q.get("api_token") {
        Some(token) => match format!("Bearer {}", token).parse::<HeaderValue>() {
            Ok(val) => val,
            Err(err) => {
                eprintln!(
                    "api_token could not be parsed into a HeaderValue: {}\ntoken: {}",
                    err, token
                );
                return Ok(with_status(
                    "api_token could not be parsed.",
                    StatusCode::BAD_REQUEST,
                ));
            }
        },
        None => {
            eprintln!("request failed: no api_token specified.");
            return Ok(with_status(
                "no api_token specified.",
                StatusCode::BAD_REQUEST,
            ));
        }
    };

    let mut domains = match q.get("domains") {
        Some(val) => val,
        None => {
            eprintln!("request failed: no domain given.");
            return Ok(with_status("no domain given.", StatusCode::BAD_REQUEST));
        }
    }
    .split(',')
    .map(|s| s.to_string())
    .collect::<Vec<String>>();

    if domains.is_empty() {
        eprintln!("request failed: no domain given.");
        return Ok(with_status("no domain given.", StatusCode::BAD_REQUEST));
    }

    let ip_addrs = {
        let ipv4 = match q.get("ipv4") {
            Some(val) => String::from(val),
            None => String::from(""),
        };
        let ipv6 = match q.get("ipv6") {
            Some(val) => String::from(val),
            None => String::from(""),
        };

        let mut ip_addrs = Vec::new();
        if !ipv4.is_empty() {
            ip_addrs.push((String::from("A"), ipv4));
        }
        if !ipv6.is_empty() {
            ip_addrs.push((String::from("AAAA"), ipv6));
        }
        ip_addrs
    };

    if ip_addrs.is_empty() {
        eprintln!("request failed: no ip address given.");
        return Ok(with_status("no ip address given", StatusCode::BAD_REQUEST));
    }

    let use_proxy = match q.get("use_proxy") {
        Some(val) => val == "true",
        None => false,
    };

    let comment = match q.get("comment") {
        Some(val) => String::from(val),
        None => String::from(""),
    };

    let tags = match q.get("tags") {
        Some(val) => String::from(val),
        None => String::from(""),
    }
    .split(',')
    .map(|s| s.to_string())
    .filter(|i| !i.is_empty())
    .collect::<Vec<String>>();

    // default to 300 (5min), enterprise zones can go down to 30, though for this case it should be set via
    // the parameter.
    let time_to_live = match q.get("time_to_live") {
        Some(val) => val.parse::<u32>().unwrap_or(300),
        None => 300,
    };

    let mut headers = HeaderMap::new();
    headers.insert("Authorization", api_token);
    headers.insert(
        "content-type",
        HeaderValue::try_from("application/json").unwrap(),
    );

    let idents = {
        let url = format!(
            "https://api.cloudflare.com/client/v4/zones/{}/dns_records",
            zone_ident
        );
        let client = reqwest::Client::new();
        let request = match client.get(url).headers(headers.clone()).build() {
            Ok(val) => val,
            Err(err) => {
                eprintln!("error while creating listing request: {}", err);
                return Ok(with_status(
                    "error while creating listing request.",
                    StatusCode::BAD_REQUEST,
                ));
            }
        };

        let result: CloudflareListingResponse = match client.execute(request).await {
            Ok(response) => {
                // parse into CloudflareResultWrapper
                match response.text().await {
                    Ok(response_text) => match serde_json::from_str(&response_text) {
                        Ok(result) => result,
                        Err(err) => {
                            eprintln!("could not parse response text: {}", err);
                            return Ok(with_status(
                                "could not parse response text.",
                                StatusCode::BAD_REQUEST,
                            ));
                        }
                    },
                    Err(err) => {
                        eprintln!("failed to get response text: {}", err);
                        return Ok(with_status(
                            "failed to get response text.",
                            StatusCode::BAD_REQUEST,
                        ));
                    }
                }
            }
            Err(err) => {
                eprintln!("request failed: {}", err);
                return Ok(with_status("request failed.", StatusCode::BAD_REQUEST));
            }
        };

        let mut idents: HashMap<String, String> = HashMap::new();
        if result.success {
            println!("got identifiers for:");
            for listing in result.result {
                #[cfg(debug_assertions)]
                println!("{}: {}", &listing.name, &listing.id);
                if domains.contains(&listing.name) {
                    // only output names for domains that should be updated.
                    #[cfg(not(debug_assertions))]
                    println!("{}", &listing.name);
                    idents.insert(listing.name, listing.id);
                }
            }
        }

        idents
    };

    // only update domains that actually exist in this zone
    domains.retain(|domain| idents.contains_key(domain));

    let client = reqwest::Client::new();
    for (record_type, ip_addr) in ip_addrs.iter() {
        for domain_name in domains.iter() {
            // the domain names get filtered earlier, so we don't have to check here.
            let ident = idents.get(domain_name).unwrap();

            // TODO?: make url customizable.
            let url = format!(
                "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}",
                zone_ident, ident
            );

            let body = CloudflareUpdateRequest {
                ip_addr: String::from(ip_addr),
                domain_name: String::from(domain_name),
                use_proxy,
                record_type: String::from(record_type),
                comment: comment.clone(),
                tags: tags.clone(),
                time_to_live,
            };

            let body = match serde_json::to_string(&body) {
                Ok(val) => val,
                Err(err) => {
                    eprintln!("failed to serialize body for update request: {}", err);
                    return Ok(with_status(
                        "failed to serialize body for update request.",
                        StatusCode::BAD_REQUEST,
                    ));
                }
            };

            let request = match client
                .put(url.clone())
                .headers(headers.clone())
                .body(body)
                .build()
            {
                Ok(val) => val,
                Err(err) => {
                    eprintln!("could not prepare request: {}", err);
                    return Ok(with_status(
                        "could not prepare request.",
                        StatusCode::BAD_REQUEST,
                    ));
                }
            };

            #[cfg(debug_assertions)]
            println!("request: {:#?}\n", request);

            match client.execute(request).await {
                Ok(res) => {
                    if cfg!(debug_assertions) {
                        println!("got response");

                        let status = res.status();
                        let url = res.url().clone();
                        let headers = res.headers().clone();
                        let body = res.text().await;
                        println!(
                            "\nstatus: {}\n\nurl: {}\n\nheaders: {:#?}\n\nbody: {:#?}\n",
                            status, url, headers, body
                        );
                    } else {
                        // TODO: show success and maybe errors/messages.
                        // let body = res.text().await;
                        // TODO: parse response.success, for now assume success on http 2xx
                        let success = res.status().is_success();
                        println!(
                            "got response for {}: {}",
                            domain_name,
                            if success { "success" } else { "error" }
                        );
                    }
                }
                Err(err) => {
                    eprintln!("request failed: {}", err);
                    return Ok(with_status("request failed", StatusCode::BAD_REQUEST));
                }
            }
        }
    }

    Ok(with_status("Ok", StatusCode::OK))
}

impl Display for CloudflareUpdateRequest {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(
            format_args!(
                "domain: {}\n\nrecord_type: {}\n\nttl: {}\n\nip_addr: {}\n\nuse_proxy: {}\n\ncomment: {}\n\ntags: {:?}\n",
                self.domain_name,
                self.record_type,
                self.time_to_live,
                self.ip_addr,
                self.use_proxy,
                self.comment,
                self.tags,
                )
            )
    }
}

impl Display for Config {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "{}:{}",
            self.addr.map(|num| num.to_string()).join("."),
            self.port
        ))
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            addr: [0, 0, 0, 0],
            port: 8888,
        }
    }
}
